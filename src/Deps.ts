/**
 * A handy pattern borrowed from deno to aggregate your dependencies and e.g. rename them, change their import structure and cautiously extend that functionality.
 *
 * This should NOT become another "Utils" module.
 */
// this _is_ the root that the restriction points to
/* eslint-disable no-restricted-imports */
import { monotonicFactory } from "ulid";
export const ulid = monotonicFactory();

export { A } from "andale";
export * from "fp-ts";
export { flow, pipe, flip, apply, identity } from "fp-ts/lib/function";
export * as HTMX from "htmx-tsx";
export { z as Zod } from "zod";
export * from "bun:sqlite";
export * from "ts-pattern";
export * as Path from "node:path";
export * from "@faker-js/faker";

export const ignore = () => {};
export const tap =
  <T>(fn: (value: T) => void) =>
  (value: T) => {
    fn(value);
    return value;
  };
