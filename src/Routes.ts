/**
 * The Web Access Layer.
 *
 * Here you should wire up the application layers functionalities to endpoints - i.e. make that functionality available to the outside world.
 */
import * as App from "App";
import { A, Path, flow, match, pipe } from "Deps";

const getPublicAsset = pipe(
  A.HTTP.context(),
  A.HTTP.Context.withCaching(),
  A.HTTP.handle(({ request, cache }) => {
    const file = Bun.file(new URL(request.url).pathname.slice(1));
    const ext = Path.extname(request.url);
    return cache.etag(
      new Response(file, {
        headers: {
          "Content-Type": match(ext)
            .with("css", () => "text/css")
            .with("js", () => "text/javascript")
            .with("html", () => "text/html")
            .otherwise(() => "text/plain"),
        },
      }),
      file.size + file.lastModified.toString().slice(0, -5),
    );
  }),
);

export const createRoutes = () =>
  A.routes({
    "": {
      get: App.getApp(),
    },
    persons: {
      get: App.getPersons(),
    },
    person: {
      post: App.addPerson(),
      put: App.updatePerson(),
    },
    public: {
      [A.wildcard]: getPublicAsset,
    },
  });

export type Routes = ReturnType<typeof createRoutes>;

export const create = flow(createRoutes, A.create);
