/**
 * The application layer.
 *
 * This layer glues together the Web, Repository and Domain layers.
 */
import { A, HTMX, flow, pipe, Zod, ulid, faker } from "Deps";
import { type Routes } from "Routes";

export type Person = {
  id: string;
  firstname: string;
  lastname: string;
  age: number;
  income: number;
};

let persons = Array.from(
  { length: 100 },
  (): Person => ({
    id: ulid(),
    firstname: faker.person.firstName(),
    lastname: faker.person.lastName(),
    age: faker.number.int({ min: 18, max: 99 }),
    income: faker.number.float({ precision: 0.01, min: 35_000, max: 120_000 }),
  }),
);

const url = A.url<Routes>;

function Row({ id, firstname, lastname, age, income }: Person) {
  return (
    <tr>
      <td>
        <form
          id={id}
          hx-put={url(["PUT /person", { id }])}
          hx-swap="outerHTML"
          hx-trigger="change from:closest tr"
          hx-target="closest tr"
        ></form>
        <input form={id} value={firstname} name="firstname" />
      </td>
      <td>
        <input form={id} value={lastname} name="lastname" />
      </td>
      <td>
        <input type="number" name="age" form={id} value={age.toString()} />
      </td>
      <td style={{ position: "relative" }}>
        <input
          type="number"
          name="income"
          step="0.01"
          form={id}
          value={income.toFixed(2)}
          style={{
            paddingLeft: "2rem",
          }}
        />
        <span
          style={{
            position: "absolute",
            top: "50%",
            left: "2rem",
            transform: "translateY(-50%)",
          }}
        >
          €
        </span>
      </td>
    </tr>
  );
}

const sortDirectionSchema = Zod.union([
  Zod.literal("asc"),
  Zod.literal("desc"),
]);
type SortDirection = Zod.infer<typeof sortDirectionSchema>;

const tablePropsSchema = Zod.object(
  // sorting
  {
    "sort-firstname": sortDirectionSchema.optional(),
    "sort-lastname": sortDirectionSchema.optional(),
    "sort-age": sortDirectionSchema.optional(),
    "sort-income": sortDirectionSchema.optional(),
  } as {
    [k in keyof PersonForm as `sort-${k}`]: Zod.ZodOptional<
      typeof sortDirectionSchema
    >;
  },
).extend({
  page: Zod.coerce
    .number()
    .int("fractional pages aren't implemented")
    .min(0, "no pages before first page")
    .optional()
    .default(0),
  pageSize: Zod.coerce
    .number()
    .int("can't return part of a row")
    .optional()
    .default(10),
});
type TableProps = Zod.infer<typeof tablePropsSchema>;

function Table({
  // currently a bug in htmx-tsx, children are always part of props
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  children: _,
  ...props
}: TableProps & Pick<JSX.Element, "children">) {
  const { page, pageSize, ...sorting } = props;

  // ignore multiple entries for now, no multi-level sorting
  const [sortClause] = Object.entries(sorting).map(
    ([key, value]) =>
      [key.replace("sort-", "") as keyof PersonForm, value] as const,
  );

  const compare =
    sortClause == null
      ? () => 0
      : (a: PersonForm, b: PersonForm) => {
          const [by, direction] = sortClause;
          const aVal = a[by];
          const bVal = b[by];
          return (
            (aVal == bVal ? 0 : aVal < bVal ? -1 : 1) *
            (direction === "asc" ? 1 : -1)
          );
        };

  const entries = persons
    .slice(page * pageSize, (page + 1) * pageSize)
    .sort(compare);

  function Header({
    children,
    prop,
  }: Pick<JSX.Element, "children"> & { prop: keyof PersonForm }) {
    const currentDirection = props[`sort-${prop}`];
    function SortButton({ direction }: { direction: SortDirection }) {
      return (
        <a
          hx-get={url([
            "GET /persons",
            {
              page,
              pageSize,
              [`sort-${prop}`]: direction,
            },
          ])}
          hx-target="closest .table"
          hx-swap="outerHTML"
        >
          Sort {direction.toUpperCase()}
        </a>
      );
    }

    return (
      <th style={{ position: "relative", textAlign: "center" }}>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            gap: "1rem",
          }}
        >
          {children}
          {currentDirection === "asc" ? (
            <SortButton direction="desc" />
          ) : (
            <SortButton direction="asc" />
          )}
        </div>
        <div
          class="drag-handle"
          style={{
            borderRight: "2px solid var(--form-element-border-color)",
            height: "100%",
            position: "absolute",
            right: "0",
            top: "0",
            cursor: "col-resize",
          }}
          _="
        on load 
          set :table to closest <table/> 
          set :header to closest <th/>
          set :dragging to false
        end
        on mousedown set :table's *user-select to 'none' then set :dragging to true end
        on mouseup from document set :table's *user-select to 'unset' then set :dragging to false end
        on mousemove[:dragging == true] from document set :header's *width to (:header's offsetWidth + event.movementX) end
        "
        ></div>
      </th>
    );
  }

  return (
    <div class="table">
      <table>
        <thead>
          <tr>
            <Header prop="firstname">Firstname</Header>
            <Header prop="lastname">Lastname</Header>
            <Header prop="age">Age</Header>
            <Header prop="income">Income</Header>
          </tr>
        </thead>
        <tbody>{entries.map(Row)}</tbody>
      </table>
      <div style={{ display: "flex", gap: "5rem" }}>
        <button
          disabled={page === 0}
          hx-get={url([
            "GET /persons",
            {
              ...props,
              page: page - 1,
            },
          ])}
          hx-target="closest .table"
          hx-swap="outerHTML"
        >
          «
        </button>
        <button
          disabled={page === persons.length / pageSize - 1}
          hx-get={url([
            "GET /persons",
            {
              ...props,
              page: page + 1,
            },
          ])}
          hx-target="closest .table"
          hx-swap="outerHTML"
        >
          »
        </button>
      </div>
    </div>
  );
}

function App() {
  return (
    <html>
      <head>
        <meta charset="utf-8"></meta>
        <script src="https://unpkg.com/htmx.org@1.9.9"></script>
        <script src="https://unpkg.com/hyperscript.org@0.9.12"></script>
        <link rel="stylesheet" href="public/index.css" />
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/@picocss/pico@1/css/pico.min.css"
        />
      </head>
      <body>
        <main
          class="container"
          hx-trigger="load"
          hx-get={url(["GET /persons", { page: 0, pageSize: 10 }])}
          hx-swap="beforeend"
        >
          <h1>HTMX Table Form</h1>
        </main>
      </body>
    </html>
  );
}

const createMiddleware = () =>
  pipe(
    A.HTTP.context(),
    A.HTTP.Middleware.html(),
    A.Context.extend(({ html }) => ({
      send: {
        html: flow(HTMX.toHtml, html),
      },
    })),
  );

export const getApp = flow(
  createMiddleware,
  A.HTTP.handle(({ send }) => send.html(App())),
);

export const getPersons = flow(
  createMiddleware,
  A.HTTP.Validate.query(tablePropsSchema),
  A.HTTP.handle(({ send, query }) => {
    return send.html(<Table {...query} />);
  }),
);

const personFormSchema = Zod.object({
  firstname: Zod.string().min(1, "firstname cannot be empty"),
  lastname: Zod.string().min(1, "lastname cannot be empty"),
  age: Zod.coerce.number().min(0, "age cannot be negative"),
  income: Zod.coerce.number().min(0, "salary must not be negative"),
});
type PersonForm = Zod.infer<typeof personFormSchema>;

export const addPerson = flow(
  createMiddleware,
  A.HTTP.Validate.body(personFormSchema),
  A.HTTP.handle(({ send, body }) => {
    const person: Person = { ...body, id: ulid() };
    persons.push(person);
    return send.html(Row(person));
  }),
);

export const updatePerson = flow(
  createMiddleware,
  A.HTTP.Validate.query(
    Zod.object({
      id: Zod.string().min(1, "id is required"),
    }),
  ),
  A.HTTP.Validate.body(personFormSchema),
  A.HTTP.handle(({ send, body, query }) => {
    const next: Person = { ...query, ...body };
    persons = persons.map((person) => (person.id === query.id ? next : person));
    return send.html(Row(next));
  }),
);
