/**
 * the programs entry point.
 *
 * Set up the connections to the adapters (ports & adapters, e.g. databases or incoming requests) here.
 *
 * This should be the only module with hardcoded (or process bound) values.
 */
import * as Routes from "Routes";

const { handle } = Routes.create();

const server = Bun.serve({
  fetch: handle,
});

console.log(`listening on ${server.hostname}:${server.port}`);
