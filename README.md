# HTMX table form

Just a demo repository that there's not really a limit of what you can do with HTMX, at least not technically.

## Features

- resizable columns in 8 lines of hyperscript
- sortable columns
- one form per row
- only loads the data it needs (+ a bit of overhead)

## What about the sensibility of this use case?

Well... I think there's something to be said either way. The initial (cold, uncached) transfers ~66Kb, which is probably less than your average JS Frontend Framework alone. Subsequent full reloads transfer only ~12Kb, which is quite nice.

But for every interaction the whole table is reloaded from the server and having the full markup included for every row is definitely more than just transferring the data and building the UI on the frontend, so y'know... tradeoffs. I'll say this though, this has taken me about ~3 hours and rarely have I created a table like that from scratch (no libs involved) in any other framework without having to load the full dataset from the backend.

## Notes

This project has been largely based on the [BASH stack](https://gitlab.com/hesxenon/bash-stack/-/tree/main)
