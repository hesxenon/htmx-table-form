/* eslint-env node */

// eslint-disable-next-line @typescript-eslint/no-var-requires
const { dependencies, devDependencies } = require("./package.json");

module.exports = {
  extends: ["eslint:recommended", "plugin:@typescript-eslint/recommended"],
  parser: "@typescript-eslint/parser",
  plugins: ["@typescript-eslint"],
  root: true,
  rules: {
    "no-restricted-imports": [
      "error",
      {
        paths: [
          ...Object.keys(dependencies).map((name) => ({
            name,
            message: "please use Deps.ts instead",
          })),
          ...Object.keys(devDependencies).map((name) => ({
            name,
            message: "dev dependencies should not be imported in production code",
          })),
        ],
      },
    ],
    "@typescript-eslint/no-namespace": "off", // premise for this rules reasoning is broken, namespaces AREN'T modules. E.g. nested modules aren't possible.
  },
};
